$( document ).ready(function() {
    $('#register').click(function(event){
        event.preventDefault();
        var username = $('#username').val();
        var email = $('#email').val();
        var password = $('#password1').val();
        
        $.ajax({
                type: 'POST',
                url: '/user/submitNewUser/',
                dataType: "json", 
                data: 'username=' + username + '&email=' + email + '&password=' + password, 
                success: function(data, textStatus, jqXHR){
                    if(data.status == "success")
                        window.location.replace("/");
                }
         });
    });
});