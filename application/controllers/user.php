<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	public function index()
	{
		$this->load->view('profile_view');
	}
        
        public function create()
	{
		$this->load->view('create_user_view');
	}
        
        public function view()
        {
            $this->load->view('user_view');
        }
        
        public function submitNewUser(){
            error_log('submitting user');
            $this->load->model('auth_model');
            $this->load->model('user_model');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $salt = $this->auth_model->generateSalt();
            $hash = $this->auth_model->hashPassword($password);
            
            $userData = array(
                "username" => $username,
                "email_address" => $email,
                "password" => $hash,
                "salt" => $salt,
                "ip_address" => $this->session->userdata('ip_address')
            );
            error_log(print_r($userData,true));
            $userExistsData = $this->user_model->checkUserExistsByEmail($email);
            error_log("user data: " . print_r($userExistsData,true));
            if($userExistsData != FALSE){
                error_log("users exists!!!!");
               echo json_encode(array("status" => "error", "errormessage" => "Email Address already in use"));
               die();
            }
            
            $userId = $this->user_model->addUser($userData); 
            $userData['password'] = $password;
            if(!empty($userId)){
                $this->auth_model->authenticate($userData);
                echo json_encode(array("status" => "success"));
                die();
            }
            else
                echo json_encode(array("status" => "error", "errormessage" => "Failed to add user"));

        }
}