<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <title>Websockets Chat - Profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Portfolio project showing skills with Node.js, PHP, Javascript, jQuery, AJAX and API's">
        <meta name="author" content="Patrick Burns">
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/css/bootstrap-responsive.min.css" rel="stylesheet">
        <script type="text/javascript">
            if (window.location.hash && window.location.hash == '#_=_') {
                if (window.history && history.pushState) {
                    window.history.pushState("", document.title, window.location.pathname);
                } else {
                    // Prevent scrolling by storing the page's current scroll offset
                    var scroll = {
                        top: document.body.scrollTop,
                        left: document.body.scrollLeft
                    };
                    window.location.hash = '';
                    // Restore the scroll offset, should be flicker free
                    document.body.scrollTop = scroll.top;
                    document.body.scrollLeft = scroll.left;
                }
            }
        </script>
    </head>
    <body>    
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Websockets Chat</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#">Source</a></li>
                    <li><a href="http://www.linkedin.com/in/burnsforce">Contact</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://simpleurl.co">URL Shortener</a></li>
                            <li><a href="http://burnsforcedevelopment.com/linkbaitgenerator/">Link Bait Generator</a></li>
                            <li><a href="http://chat.burnsforcedevelopment.com/">Web Sockets Chat</a></li>
                            <li><a href="http://burnsforcedevelopment.com/citysay/">Kansas City Says</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                    if ($this->session->userdata('logged_in') != 1) {
                        printf('<li><a href="/register/" data-toggle="modal">Register</a></li>');
                        printf('<li><a href="/login/" data-toggle="modal">Login</a></li>');
                    } else {
                        $avatarLink = '<img src="http://graph.facebook.com/' . $this->session->userdata('username') . '/picture" class="header-avatar" />';
                        printf('<li class="dropdown">');
                        printf('<a href="#" class="dropdown-toggle" data-toggle="dropdown">');
                        printf('<span>%s %s</span>', $avatarLink, $this->session->userdata('username'));
                        printf('</a>');
                        printf('<ul class="dropdown-menu">');
                        printf('<li><a href="/user"><i class="icon-user"></i> Profile</a></li>');
                        printf('<li class="divider"></li>');
                        printf('<li><a href="/logout"><i class="icon-off"></i> Logout</a></li>');
                        printf('</ul>');
                        printf('</li>');
                    }
                    ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <img src="https://graph.facebook.com/<?php echo $this->session->userdata('username'); ?>/picture?type=large" class="img-rounded img-responsive" style="width:380px; height:170px; margin-left: 17px;"/>
                                
                            </div>
                            <div class="col-sm-6 col-md-8">
                                <h4><?php echo $this->session->userdata('username'); ?></h4>
                                <p>
                                    <i class="glyphicon glyphicon-envelope"></i><?php echo $this->session->userdata('email'); ?>
                                    <br />
                                <!-- Split button -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary">
                                        Social</button>
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span><span class="sr-only">Social</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="https://plus.google.com/+Jquery2dotnet/posts">Google +</a></li>
                                        <li><a href="https://www.facebook.com/jquery2dotnet">Facebook</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Github</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/nodejs/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.min.js"></script>
    <script src="/assets/js/login.js"></script>
</html>