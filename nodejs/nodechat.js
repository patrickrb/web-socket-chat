var io = require('socket.io').listen(8082,{
  'log level':1
});

io.sockets.on('connection', function(socket){
    socket.emit('startup');
    
    socket.on('newMessage', function (data) {
        io.sockets.emit('updateChat', data);
    });
});