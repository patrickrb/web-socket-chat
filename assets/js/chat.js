$( document ).ready(function() {
    
    socket = io.connect('http://chat.burnsforcedevelopment.com:8082');
    
    socket.on('startup',function(){
        $.jGrowl('<i class="icon-ok-sign" style="color:green;font-size:14px;padding-right:3px;"></i>Connected to websocket server.', { position: 'bottom-right' });
    });
        
    socket.on('updateChat', function (messageData) {
        console.log(messageData);
        $(messageData.messageHtml).appendTo('.chat');
        $(".panel-body").animate({ scrollTop: $('.panel-body')[0].scrollHeight}, 1000);
    });
        
    $(".panel-body").animate({ scrollTop: $('.panel-body')[0].scrollHeight}, 1000);
    
    $('.panel-body').on('scroll', function(){
        if ($(this).scrollTop() === 0) {
            var pageNum = parseInt($('#pageNum').val());
            $('#loadingMessage').remove();
            $('.chat').prepend('<li id="loadingMessage" class="left clearfix">'
                                    +'<div class="chat-body clearfix">'
                                        +'<div class="header">'
                                            +'<h4 style="text-align:center;"><i class="fa fa-refresh fa-spin"></i> Loading More Messages</h4>'
                                        +'</div>'
                                    +'</div>'
                                +'</li>');
            $.ajax({
                type: 'POST',
                url: '/message/getPaginated/' + pageNum,
                dataType: "json", 
                data: 'message=' + message, 
                success: function(data, textStatus, jqXHR){
                    if(data.status === "success"){
                        $('#loadingMessage').fadeOut('slow', function(){
                            $(this).remove();
                            $.each(data.messageData, function(index, message) { 
                                $('.chat').prepend(message.messageHtml);
                            }); 
                            $('#pageNum').val(pageNum +1);
                        });
                    }
                }
            });
        }
    });
    
    $("#message").keypress(function (e) {
        if (e.keyCode === 13){
            submitMessage();
        }
    });


    $('#submitNewMessage').click(function(event){
        event.preventDefault();
        submitMessage();
    });
});

function submitMessage(){ 
    var message = $('#message').val();
    var loggedIn = $('#loggedIn').val();
    if($('#submitNewMessage').hasClass('active')){
        return false;
    }
    if(loggedIn === 'false'){
        $.jGrowl('<i class="glyphicon glyphicon-ban-circle" style="color:red;font-size:14px;padding-right:3px;"></i> Please log in to join the conversation', { position: 'bottom-right' }); 
       return false;
    }
    if(message === ''){
        $.jGrowl('<i class="glyphicon glyphicon-ban-circle" style="color:red;font-size:14px;padding-right:3px;"></i> Please enter a message to submit', { position: 'bottom-right' }); 
        return false;
    }
    $('#submitNewMessage').toggleClass('active');
    $.ajax({
        type: 'POST',
        url: '/message/submit/',
        dataType: "json", 
        data: 'message=' + message, 
        success: function(data, textStatus, jqXHR){
            $('#message').val('');
            if(data.status === "success"){
                console.log('inserted the following data: ');
                console.log(data);
                socket.emit('newMessage',  data);
            }
            else{
                $.jGrowl('<i class="glyphicon glyphicon-ban-circle" style="color:red;font-size:14px;padding-right:3px;"></i>' + data.errormessage, { position: 'bottom-right' }); 
            }
        }       
    });
    setTimeout(
        function(){
            $('#submitNewMessage').toggleClass('active');
        }, 500
    );
}