<?php
class Message_model extends CI_Model {
function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function insertMessage($messageData){
        error_log(print_r($messageData,true));
        error_log('inserting message');
        $this->db->insert('messages', $messageData); 
        return $this->db->insert_id();
    }
    
    function getChatMessages($limit, $start){
         $this->db->limit($limit, $start);
         $this->db->order_by('message_id', "desc"); 
         $query = $this->db->get('messages');
         return $query->result_array();
    }
    
    function generateMessageHtml($messageData){
        $formattedDate = $this->convertTimeStamp($messageData['datetime']);
        if($formattedDate == "0 seconds ago"){
            $formattedDate = 'just now';
        }
        $messageHtml = '<li class="left clearfix">'
                            .'<span class="chat-img pull-left">'
                                .'<img src="'. $messageData['avatar'] . '" alt="User Avatar" class="img-circle" />'
                            .'</span>'
                            .'<div class="chat-body clearfix">'
                                .'<div class="header">'
                                    .'<strong class="primary-font">' . $messageData['username'] . '</strong>'
                                    .'<small class="pull-right  text-muted"><span class="glyphicon glyphicon-time"></span>' . $formattedDate .'</small>'
                                .'</div>'
                                .'<p>'
                                    .$messageData['message']
                                .'</p>'
                            .'</div>'
                        .'</li>';
        
        return $messageHtml;
    }
    
    function convertTimeStamp($time){
       $time = strtotime($time);
       $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
       $lengths = array("60","60","24","7","4.35","12","10");

       $now = time();

           $difference = $now - $time;
           $tense = "ago";

       for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
           $difference /= $lengths[$j];
       }

       $difference = round($difference);

       if($difference != 1) {
           $periods[$j].= "s";
       }

       return "$difference $periods[$j] ago ";
    }
	
}